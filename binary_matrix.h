/*
 * binary_matrix.h
 * Name / StudentID
 * An implementation of a Binary Matrix
 */

#ifndef _BINARY_MATRIX_H_
#define _BINARY_MATRIX_H_

typedef struct {
    int num_rows;
    int num_cols;
    int** data;
} BinaryMatrix;

BinaryMatrix* ConstructBinaryMatrix(int num_rows, int num_cols);

#endif /* _BINARY_MATRIX_H_ */
