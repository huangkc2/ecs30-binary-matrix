#include "binary_matrix.h"

#include <stdio.h>
#include <stdlib.h>
#define EXIT_FAILURE    1

typedef struct {
    int num_rows;
    int num_cols;
    int** data;
} BinaryMatrix;

int main(){

return 0;

} 

BinaryMatrix* ConstructBinaryMatrix(int num_rows, int num_cols);

BinaryMatrix* ConstructBinaryMatrix(int num_rows, int num_cols) {

if (num_rows <= 0 || num_cols <= 0) {
            printf("Error in CreateMatrix: number of rows and columns must be positive.");
            exit(1);
    } else {
            int i,x;
            BinaryMatrix* A;
            A->num_rows = num_rows;
            A->num_cols = num_cols;

            A->data = (int**) malloc(A->num_rows * sizeof(int*));

            for (i = 0; i < num_rows; i++) {
                    (A->data)[i] = malloc(A->num_cols * sizeof(int*));
                    for (x = 0; x < A->num_cols; x++) {
                            (A->data)[i][x] = 0;
                    }
            }

            return A;
    }

}

/* Delete this comment and implement this function
void DeleteBinaryMatrix(BinaryMatrix* M) {




}
*/

/* Delete this comment and implement this function
void UpdateEntry(BinaryMatrix* M, int row, int col, int content) {




}
*/

/* Delete this comment and implement this function
int IsMatrixIndex(BinaryMatrix* M, int row, int col) {




}
*/

/* Delete this comment and implement this function
void PrintMatrix(BinaryMatrix* M) {




}
*/
